package main

import "fmt"

type bot interface {
	// any type that is passed as a receiver to getGreeting is an "extension" of type bot
	getGreeting() string
}

type englishBot struct{}
type spanishBot struct{}

func main() {
	eb := englishBot{}
	sb := spanishBot{}

	printGreeting(eb)
	printGreeting(sb)
}

func printGreeting(b bot) {
	fmt.Println(b.getGreeting())
}

func (englishBot) getGreeting() string {
	// Very custom logic for generating an english greeting
	return "Hi There"
}

// can omit the variable name if it's not used inside the function
func (spanishBot) getGreeting() string {
	return "Hola!"
}
