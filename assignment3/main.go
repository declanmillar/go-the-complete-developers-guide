package main

import (
	"fmt"
	"io"
	"os"
)

// My implementation
// func main() {
// 	args := os.Args
// 	file, err := os.Open(args[1])
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	data := make([]byte, 99999)
// 	count, err := file.Read(data)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("read %d bytes: %q\n", count, data[:count])
// }

// Instructor implementation
func main() {
	f, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	io.Copy(os.Stdout, f)
}
