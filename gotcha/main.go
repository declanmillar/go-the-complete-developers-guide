package main

import "fmt"

func main() {
	mySlice := []string{"Hi", "There", "How", "Are", "You"}

	updateSlice(mySlice)

	fmt.Println(mySlice)
}

func updateSlice(s []string) {
	s[0] = "Bye"
}

// Go is a 'pass by value language' so by default a copy will be made, unless you specify a pointer
// However, in the code above, using a struct, we've modified the original value! Gotcha!

// the slice is copied, however the slice contains 3 other objects: 'length' 'cap' 'ptr to head'
// the last object here points to the primitive array within the slice
// therefore, the pointer is still points to the original array, so this is modified inside the function

// this behaviour is not limited to slices, other objects contain pointers to the underlying data structure.
// these are known as 'reference type' objects

// value types: int, float, string, bool, structs  -- Use pointers to change these things in a function
// reference types: slices, maps, channels, pointers, functions -- Don't worry about pointers with these
